# This file is part of racket-mike.

# racket-mike is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# racket-mike is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-mike.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later


# Temporarily add to "PATH" common directories in which "racket.exe" could
# be located. Remember to later (or now if "racket.exe" is somewhere else)
# add the directory containing "racket.exe" to "PATH" variable.
$env:Path += ";C:\Program Files\Racket;C:\Program Files (x86)\Racket"


$myroot = "${PSScriptRoot}"


Write-Output "Arguments: ${args}"

racket "${myroot}\mike\main.rkt" ${args}
